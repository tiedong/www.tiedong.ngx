var $ = layui.jquery,
	jQuery = layui.jquery,
	form = layui.form,
	layer = layui.layer,element = layui.element;

element.on('nav(side-scroll)', function(elem) {
	var groupID =1;
	var href = elem.data('href');

	if(href != null) {
		var rule = href.substr(1);

		$.post("{:url('admin/public/checkAuth')}?rule=" + rule + "&groupID=" + groupID, function(res) {
			if(res == 1) {
				window.location.href = href;
			}
			else {
				layer.msg('无权访问');
			}
		});
	}

});

$('.js-ajax-layer').click(function() {
	var type = $(this).data('type');
	if(!type) {
		type = 1;
	}

	var width = $(this).data('width');
	if(!width) {
		width = 'auto';
	}

	var ajaxUrl = $(this).data('url');

	if(ajaxUrl) {
		$.post(ajaxUrl, '', function(res) {
			if(res.code == 1) {
				layer.open({
					type: type,
					area: width,
					content: res.data,
				});
			} else {
				layer.open({
					type: type,
					area: width,
					content: res.data,
				});
			}
		});
	} else {
		var content = $(this).data('content');
		if(!content) {
			content = '你想干啥？';
		}
		layer.open({
			type: type,
			area: width,
			content: content,
		});
	}
});

/**
 *删除操作
 */
$('.js-ajax-delete').click(function() {
	var ajaxUrl = $(this).data('url');
	var title = $(this).data('title');
	var content = $(this).data('content');
	if(ajaxUrl) {
		layer.confirm(content, {
				title: title,
				btn: ['确认', '取消'],
				btnAlign: 'c'
			},
			function(index, layero) {
				$.post(ajaxUrl, function(res) {
					layer.msg(res.msg, '', function() {
						return false;
					});
				});
			},
			function(index) {
				layer.close(index);
			});
	} else {
		var content = $(this).data('content');
		if(!content) {
			content = '你想干啥？';
		}
		layer.msg(content);
	}
});

/**
 * 确认对话框
 */
$('.js-ajax-confirm').click(function() {
	var ajaxUrl = $(this).data('url');
	var title = $(this).data('title');
	var content = $(this).data('content');
	if(ajaxUrl) {
		layer.confirm(content, {
				title: title,
				btn: ['确认', '取消'],
				btnAlign: 'c'
			},
			function(index, layero) {
				$.post(ajaxUrl, function(res) {
					layer.msg(res.msg, '', function() {
						if(res.url) {
							window.location.href = res.url;
						} else {
							window.location.reload();
						}
					});
				});
			},
			function(index) {
				layer.close(index);
			});
	} else {
		var content = $(this).data('content');
		if(!content) {
			content = '你想干啥？';
		}
		layer.msg(content);
	}
});

/**
 * @author Eric ieyangxiaoguo@126.com
 * @desc 监听switch开关事件
 */
form.on('switch(switch)', function(data) {
	var url = $(this).data('url');
	var change = data.elem.checked; //开关是否开启，true或者false

	if(change) { //后台我需要的是0或1，所以预先在js中处理change的值
		change = 1;
	} else {
		change = 0;
	}

	$.post(url, { change: change }, function(res) {
		layer.msg(res.msg);
	});
});

/**
 * @author Eric www.zzuyxg.top
 * @desc 监听普通的general表单，不需要另外的验证
 */
form.on('submit(general)', function(data) { //其中general是我定义的过滤器
	$.post(data.action, data.field, function(res) {
		layer.msg(res.msg, {}, function() {
			if(res.url) { //如果返回值有需要跳转的url则跳转，否则重载当前页面
				window.location.href = res.url;
			} else {
				window.location.reload();
			}
		});
	});
	return false;
});

/**
 * @author Eric ieyangxiaoguo@126.com
 * @desc 图片弹窗，需要展示弹窗图片的地方都可以给img添加js-image-layer类
 */
$('.js-image-layer').click(function() {
	var src = $(this).attr('src');
	console.log(src);
	layer.open({
		type: 1,
		title: '图片查看',
		closeBtn: 1,
		area: ['600px', ''],
		maxmin: false,
		fixed: true,
		skin: 'layui-layer-nobg', //没有背景色
		shadeClose: true,
		content: '<img width="600" src="' + src + '"/>'
	});
});

//重新刷新页面，使用location.reload()有可能导致重新提交
function reloadPage(win) {
	var location = win.location;
	location.href = location.pathname + location.search;
}

/**
 * 读取cookie
 * @param name
 * @returns
 */
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while(c.charAt(0) == ' ') {
			c = c.substring(1, c.length);
		}
		if(c.indexOf(nameEQ) == 0) {
			return c.substring(nameEQ.length, c.length);
		}
	}
	return null;
}

/**
 * 设置cookie
 */
function setCookie(name, value, days) {
	var argc = setCookie.arguments.length;
	var argv = setCookie.arguments;
	var secure = (argc > 5) ? argv[5] : false;
	var expire = new Date();
	if(days == null || days == 0) days = 1;
	expire.setTime(expire.getTime() + 3600000 * 24 * days);
	document.cookie = name + "=" + escape(value) + ("; path=/") + ((secure == true) ? "; secure" : "") + ";expires=" + expire.toGMTString();
}