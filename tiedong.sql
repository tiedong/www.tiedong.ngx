/*
 Navicat Premium Data Transfer

 Source Server         : 本地库
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : tiedong

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 24/05/2021 14:04:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for y_ad
-- ----------------------------
DROP TABLE IF EXISTS `y_ad`;
CREATE TABLE `y_ad`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:显示;0:不显示',
  `list_order` int(10) UNSIGNED NOT NULL DEFAULT 9999 COMMENT '排序',
  `type` tinyint(1) UNSIGNED NULL DEFAULT NULL COMMENT '广告类型，1、文字链接；2、图片链接；3、视频；4、图文',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '广告链接地址',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '链接标题',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接图标',
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频地址',
  `target` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文字介绍',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `link_visible`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '广告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_ad
-- ----------------------------
INSERT INTO `y_ad` VALUES (1, 1, 9999, NULL, 'http://www.thinkphp.cn', 'ThinkPHP', '', '', '_blank', '');

-- ----------------------------
-- Table structure for y_admin
-- ----------------------------
DROP TABLE IF EXISTS `y_admin`;
CREATE TABLE `y_admin`  (
  `id` tinyint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_login` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `login_pass` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录密码；password加密',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录ip',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '用户状态 0：禁用； 1：正常 ；2：未验证',
  `group_id` mediumint(8) UNSIGNED NOT NULL COMMENT '权限组ID',
  `create_time` datetime(0) NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_login_key`(`admin_login`) USING BTREE,
  INDEX `user_nicename`(`nick_name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 CHECKSUM = 1 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_admin
-- ----------------------------
INSERT INTO `y_admin` VALUES (1, 'admin', '###14e1b600b1fd579f47433b88e8d85291', 'Administrator', '', '', '127.0.0.1', 1621703554, 1, 1, '2000-01-01 00:00:00');
INSERT INTO `y_admin` VALUES (8, 'tiedong', '###14e1b600b1fd579f47433b88e8d85291', '铁东1', '14718288797', '1095013906@qq.com', '127.0.0.1', 1621703509, 1, 2, '2000-01-01 00:00:00');

-- ----------------------------
-- Table structure for y_article
-- ----------------------------
DROP TABLE IF EXISTS `y_article`;
CREATE TABLE `y_article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` tinyint(2) UNSIGNED NULL DEFAULT NULL COMMENT '分类ID',
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'post标题',
  `user_id` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '发表者id',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT 'post状态，1已审核，0未审核',
  `is_top` tinyint(1) NOT NULL DEFAULT 0 COMMENT '置顶 1置顶； 0不置顶',
  `choice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '推荐 1推荐 0不推荐',
  `comment_status` int(2) NULL DEFAULT 1 COMMENT '评论状态，1允许，0不允许',
  `hits` int(11) NULL DEFAULT 0 COMMENT 'post点击数，查看数',
  `likes` int(11) NULL DEFAULT 0 COMMENT 'post赞数',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `published_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发布时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '删除时间',
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'seo keywords',
  `excerpt` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '摘要',
  `source` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '转载文章的来源',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'post内容',
  `content_filtered` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '处理过的文章内容',
  `thumb` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `thumbs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'post的扩展字段，保存相关扩展属性，如缩略图；格式为json',
  `post_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '分类：0为文章；1为单页',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_status_date`(`status`, `id`) USING BTREE,
  INDEX `post_author`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_article
-- ----------------------------
INSERT INTO `y_article` VALUES (1, 1, '222', 1, 1, 0, 0, 1, 10, 0, 1550888287, 1556158291, 1556158287, NULL, '3', '33', '原创', '&amp;lt;p&amp;gt;3333&amp;lt;/p&amp;gt;', NULL, '/upload/admin/20190425/5cc11752c35c1.jpg', NULL, 0);
INSERT INTO `y_article` VALUES (2, 1, '44', 1, 1, 0, 0, 1, 69, 0, 1550888309, 1550888309, 1550888302, NULL, '', '454', '原创', '&lt;p&gt;5454&lt;/p&gt;', NULL, '', NULL, 0);
INSERT INTO `y_article` VALUES (3, 1, '45454', 1, 1, 1, 1, 1, 61, 0, 1551105987, 1556158283, 1556158279, NULL, '', '', '原创', '&amp;lt;p&amp;gt;56565&amp;lt;/p&amp;gt;', NULL, '/upload/admin/20190425/5cc1174a94989.jpg', NULL, 0);
INSERT INTO `y_article` VALUES (4, 1, '如何部署YThinkCMS系统', 1, 1, 1, 1, 1, 220, 0, 1551150139, 1556158274, 1556158270, NULL, '', '如何部署YThinkCMS系统', '原创', '&amp;lt;p&amp;gt;如何部署YThinkCMS系统&amp;lt;/p&amp;gt;', NULL, '/upload/admin/20190425/5cc11741c55ad.jpg', NULL, 0);
INSERT INTO `y_article` VALUES (5, 1, '啦啦啦啦啦啦啦啦', 1, 1, 1, 1, 1, 2, 0, 1556113905, 1556158249, 1556158242, NULL, '', '啦啦啦啦啦啦啦啦', '原创啦啦啦啦啦啦啦啦', '&amp;lt;p&amp;gt;啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦&amp;lt;/p&amp;gt;', NULL, '/upload/admin/20190425/5cc11726418ec.jpg', '[{\"src\":\"\\/upload\\/admin\\/20190424\\/5cc069ebcba81.png\",\"url\":\"\",\"alt\":\"20181227171854914.jpg\"},{\"src\":\"\\/upload\\/admin\\/20190424\\/5cc069eccff28.jpg\",\"url\":\"\",\"alt\":\"shareThumb.png\"},{\"src\":\"\\/upload\\/admin\\/20190424\\/5cc069ed4ae62.png\",\"url\":\"\",\"alt\":\"\\u5c0f\\u7a0b\\u5e8f\\u7801-\\u8bed\\u97f3\\u5bfc\\u89c8.png\"}]', 0);

-- ----------------------------
-- Table structure for y_asset
-- ----------------------------
DROP TABLE IF EXISTS `y_asset`;
CREATE TABLE `y_asset`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件分类，目录',
  `file_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件名称',
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件地址',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `size` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_asset
-- ----------------------------
INSERT INTO `y_asset` VALUES (5, 'slide', 'QQ浏览器截图20210422173216.png', '/upload/slide/60a9342a896ac.png', 'image/png', 17960);
INSERT INTO `y_asset` VALUES (6, 'slide', 'QQ浏览器截图20210511190101.png', '/upload/slide/60a9352d849b1.png', 'image/png', 5763);
INSERT INTO `y_asset` VALUES (7, 'slide', 'QQ浏览器截图20210522141342.png', '/upload/slide/60a935f8a0301.png', 'image/png', 21841);

-- ----------------------------
-- Table structure for y_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `y_auth_group`;
CREATE TABLE `y_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_auth_group
-- ----------------------------
INSERT INTO `y_auth_group` VALUES (1, '超级管理员', 1, '', NULL);
INSERT INTO `y_auth_group` VALUES (2, '技术部1', 1, '3,15,10,8', '技术部1');

-- ----------------------------
-- Table structure for y_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `y_auth_rule`;
CREATE TABLE `y_auth_rule`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL DEFAULT 0,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `sort` int(10) NULL DEFAULT 0,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_auth_rule
-- ----------------------------
INSERT INTO `y_auth_rule` VALUES (1, '', '系统设置', 1, 1, '', 0, '0-1', 2, 'layui-icon layui-icon-set');
INSERT INTO `y_auth_rule` VALUES (2, NULL, '内容管理', 1, 1, '', 0, '0-2', 0, 'layui-icon layui-icon-user');
INSERT INTO `y_auth_rule` VALUES (3, NULL, '用户管理', 1, 1, '', 0, '0-3', 1, 'layui-icon layui-icon-user');
INSERT INTO `y_auth_rule` VALUES (4, 'admin/menu/index', '菜单管理', 1, 1, '', 1, '0-1-4', 1, NULL);
INSERT INTO `y_auth_rule` VALUES (7, 'admin/setting/site', '站点信息', 1, 1, '', 1, '0-1-7', 0, NULL);
INSERT INTO `y_auth_rule` VALUES (8, 'admin/auth_group/index', '权限管理', 1, 1, '', 3, '0-3-8', 3, NULL);
INSERT INTO `y_auth_rule` VALUES (9, 'admin/slide/index', '幻灯片管理', 1, 1, '', 1, '0-1-9', 5, NULL);
INSERT INTO `y_auth_rule` VALUES (10, 'admin/admin/index', '管理员管理', 1, 1, '', 3, '0-3-10', 0, NULL);
INSERT INTO `y_auth_rule` VALUES (12, 'admin/category/index', '栏目管理', 1, 1, '', 2, '0-2-12', 0, NULL);
INSERT INTO `y_auth_rule` VALUES (13, 'admin/article/index', '内容管理', 1, 1, '', 2, '0-2-13', 0, NULL);
INSERT INTO `y_auth_rule` VALUES (15, 'admin/admin/add', '添加管理员', 1, 2, '', 3, '0-3-15', 0, NULL);

-- ----------------------------
-- Table structure for y_category
-- ----------------------------
DROP TABLE IF EXISTS `y_category`;
CREATE TABLE `y_category`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `parent_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类父id',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:显示,0:不显示',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '删除时间',
  `list_order` smallint(5) UNSIGNED NOT NULL DEFAULT 100 COMMENT '排序',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类层级关系路径',
  `seo_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `list_tpl` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类列表模板',
  `one_tpl` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类文章页模板',
  `more` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '扩展属性',
  `type` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_category
-- ----------------------------
INSERT INTO `y_category` VALUES (1, 0, 1, NULL, 100, '景点', '0-1', '学院', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (4, 0, 1, NULL, 100, '攻略', '0-4', 'YThink博客', 'YThink,博客', 'YThink功能更新日志', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (5, 0, 1, NULL, 100, '酒店', '0-5', '', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (6, 0, 1, NULL, 100, '旅行社', '0-6', '', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (2, 0, 1, NULL, 100, '购物', '0-2', '开发话题', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (7, 0, 1, NULL, 100, '活动', '0-7', '', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (8, 0, 1, NULL, 100, '文明规范', '0-8', '', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (9, 0, 1, NULL, 100, '交通', '0-9', '', '', '', '', '', NULL, 1);
INSERT INTO `y_category` VALUES (3, 0, 1, NULL, 100, '餐饮', '0-3', 'YThink案例', '', '', '', '', NULL, 1);

-- ----------------------------
-- Table structure for y_nav
-- ----------------------------
DROP TABLE IF EXISTS `y_nav`;
CREATE TABLE `y_nav`  (
  `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:显示;0:不显示',
  `list_order` int(10) UNSIGNED NOT NULL DEFAULT 9999 COMMENT '排序',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接地址',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `target` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '打开方式',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接描述',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '上级导航',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `link_visible`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '前台导航' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_nav
-- ----------------------------
INSERT INTO `y_nav` VALUES (1, 1, 0, 'http://www.ythink.test', '首页', '_self', '', 0, '0-1');
INSERT INTO `y_nav` VALUES (2, 1, 1, 'http://www.ythink.test/list/1.html', '教程', '_self', '', 0, '0-2');
INSERT INTO `y_nav` VALUES (5, 1, 4, 'https://gitee.com/zzuyxg/yThink', 'yThink', '_blank', '', 0, '0-5');
INSERT INTO `y_nav` VALUES (3, 1, 2, 'http://www.ythink.test/list/2.html', '话题', '_self', '', 0, '0-3');
INSERT INTO `y_nav` VALUES (4, 1, 3, 'http://www.ythink.test/list/3.html', '案例', '_self', '', 0, '0-4');
INSERT INTO `y_nav` VALUES (8, 1, 0, 'http://www.ythink.test/list/1.html', '教程', '_blank', '', 0, '0-8');
INSERT INTO `y_nav` VALUES (6, 1, 5, 'https://gitee.com/zzuyxg/yThink/issues', 'issues', '_blank', '', 0, '0-6');
INSERT INTO `y_nav` VALUES (7, 1, 3, 'http://www.ythink.test/list/4.html', '博客', '_self', '', 0, '0-7');
INSERT INTO `y_nav` VALUES (9, 1, 0, 'http://www.ythink.test/page/5.html', '关于我们', '_blank', '', 0, '0-9');

-- ----------------------------
-- Table structure for y_option
-- ----------------------------
DROP TABLE IF EXISTS `y_option`;
CREATE TABLE `y_option`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `autoload` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否自动加载;1:自动加载;0:不自动加载',
  `option_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置名',
  `option_value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `option_name`(`option_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '全站配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_option
-- ----------------------------
INSERT INTO `y_option` VALUES (1, 1, 'site_info', '{\"site_name\":\"\\u54c8\\u54c8\\u54c8\",\"site_icp\":\"rrrrrrrrrrrr\",\"company_name\":\"\\u54c8\\u54c8\\u54c8\",\"address\":\"\",\"phone\":\"\",\"worktime\":\"\",\"qq\":\"\",\"site_email\":\"ieyangxiaoguo@126.com\",\"site_seo_title\":\"YThink\",\"site_seo_keywords\":\"thinkphp,lauyui,PHP\\u6846\\u67b6\",\"site_seo_description\":\"\\u57fa\\u4e8eThinkPHP5.1\\u548clayui\\u4e24\\u79cd\\u6846\\u67b6\\uff0c\\u521b\\u5efaYThink\\u7cfb\\u7edf\\uff0c\\u8ba1\\u5212\\u662f\\u6ee1\\u8db3\\u4e00\\u822c\\u5c0f\\u4f01\\u4e1a\\u6216\\u4e2a\\u4eba\\u7ad9\\u70b9\\u5bf9\\u5b98\\u7f51\\u4ee5\\u53ca\\u5c0f\\u7a0b\\u5e8f\\u7684\\u9700\\u6c42\",\"site_analytics\":\"\",\"site_copyright\":\"\",\"logo\":\"\\/upload\\/site\\/60a86d1465f34.png\",\"wechat\":\"\"}');

-- ----------------------------
-- Table structure for y_slide
-- ----------------------------
DROP TABLE IF EXISTS `y_slide`;
CREATE TABLE `y_slide`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '幻灯片分类',
  `idname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '幻灯片分类标识',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分类备注',
  `status` int(2) NOT NULL DEFAULT 1 COMMENT '状态，1显示，0不显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cat_idname`(`idname`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '幻灯片分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_slide
-- ----------------------------
INSERT INTO `y_slide` VALUES (1, '首页轮播图', 'index_carousel', '首页轮播图444', 1);
INSERT INTO `y_slide` VALUES (2, '测试', 'test', '测试', 1);

-- ----------------------------
-- Table structure for y_slide_item
-- ----------------------------
DROP TABLE IF EXISTS `y_slide_item`;
CREATE TABLE `y_slide_item`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slide_id` int(11) NOT NULL COMMENT '幻灯片分类 id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '幻灯片名称',
  `thumb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '幻灯片图片',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '幻灯片链接',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '幻灯片描述',
  `status` int(2) NOT NULL DEFAULT 1 COMMENT '状态，1显示，0不显示',
  `list_order` int(10) NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `slide_cid`(`slide_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '幻灯片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_slide_item
-- ----------------------------
INSERT INTO `y_slide_item` VALUES (5, 1, '3333', '/upload/admin/slide/5cc0f65871354.jpg', 'http://www.zzuyxg.top', '', 1, 0);
INSERT INTO `y_slide_item` VALUES (3, 1, 'YThink1.0', '/upload/admin/slide/5cc0f6465e3c0.jpg', 'https://www.zzuyxg.top', '', 1, 0);
INSERT INTO `y_slide_item` VALUES (6, 1, '444', '/upload/admin/slide/5cc0f66b6a8c8.jpg', 'http://www.zzuyxg.top', '', 1, 0);
INSERT INTO `y_slide_item` VALUES (4, 1, '333', '/upload/admin/slide/5cc0f64e02638.jpg', 'http://www.zzuyxg.top', '', 1, 0);
INSERT INTO `y_slide_item` VALUES (7, 1, '测试', '/upload/slide/60a9352d849b1.png', 'http://www.baidu.com', 'fefwef', 1, 0);
INSERT INTO `y_slide_item` VALUES (8, 1, 'hhha23', '/upload/slide/60a935f8a0301.png', 'http://www.baidu.com', 'wdqwdq12', 1, 0);

-- ----------------------------
-- Table structure for y_sns
-- ----------------------------
DROP TABLE IF EXISTS `y_sns`;
CREATE TABLE `y_sns`  (
  `user_id` int(11) NOT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方平台昵称',
  `open_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'openID',
  `union_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'unionid',
  `vendor` tinyint(1) UNSIGNED NOT NULL COMMENT '第三方平台类型',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  UNIQUE INDEX `sns_user_id_unique`(`user_id`) USING BTREE,
  INDEX `sns_open_id_index`(`open_id`) USING BTREE,
  INDEX `sns_vendor_index`(`vendor`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_sns
-- ----------------------------

-- ----------------------------
-- Table structure for y_user
-- ----------------------------
DROP TABLE IF EXISTS `y_user`;
CREATE TABLE `y_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `mobile` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `login_pwd` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `real_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `nick_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `birthday` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '生日',
  `mark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户备注',
  `avatar` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户头像',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '邮箱地址',
  `create_time` int(10) UNSIGNED NOT NULL COMMENT '注册时间',
  `create_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注册IP',
  `last_login_time` int(11) UNSIGNED NOT NULL COMMENT '最后一次登录时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后一次登录ip',
  `now_money` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '用户余额',
  `brokerage_price` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '佣金金额',
  `integral` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '用户剩余积分',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1为正常，0为禁止',
  `level` tinyint(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `spread_uid` int(10) UNSIGNED NOT NULL COMMENT '推广员id',
  `spread_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '推广员关联时间',
  `spread_count` int(11) NULL DEFAULT 0 COMMENT '下级人数',
  `clean_time` int(11) NULL DEFAULT 0 COMMENT '清理会员时间',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `login_type` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登陆类型，h5,WeChat,WxApp',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `spreaduid`(`spread_uid`) USING BTREE,
  INDEX `level`(`level`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_user
-- ----------------------------

-- ----------------------------
-- Table structure for y_user_relations
-- ----------------------------
DROP TABLE IF EXISTS `y_user_relations`;
CREATE TABLE `y_user_relations`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fx_level` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '分销等级，1一级下线；2二级下线',
  `user_id` int(10) NULL DEFAULT 0 COMMENT '用户ID',
  `child_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '下级用户ID',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '关联时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'User上下级关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of y_user_relations
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
