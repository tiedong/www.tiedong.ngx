<?php
// +----------------------------------------------------------------------
// | 日志设置
//  ThinkPHP对系统的日志按照级别来分类记录，按照PSR-3日志规范，日志的级别从低到高依次为：
//  debug, info, notice, warning, error, critical, alert, emergency
//  ThinkPHP额外增加了一个sql日志级别仅用于记录SQL日志（并且仅当开启数据库调试模式有效）。
// +----------------------------------------------------------------------
return [
    // 日志记录方式，内置 file socket 支持扩展
    'type'        => 'File',
    // 日志保存目录
    'path'        => '',
    // 日志记录级别
    'level'       => ['error','alert'],
    // 单文件日志写入
    'single'      => false,
    // 独立日志级别
    'apart_level' => ['error','sql'],
    // 最大日志文件数量
    'max_files'   => 10,
    // 是否关闭日志写入
    'close'       => false,
];
