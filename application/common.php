<?php
use think\captcha\Captcha;
use PHPMailer\PHPMailer\PHPMailer;

// 应用公共文件
/**
 * 判断是否为手机访问
 *
 * @return boolean
 */
function is_mobile()
{
    static $is_mobile;

    if (isset($is_mobile))
        return $is_mobile;

        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            $is_mobile = false;
        } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false || // many mobile devices (all iPhone, iPad, etc.)
            strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mobi') !== false) {
                $is_mobile = true;
        } else {
            $is_mobile = false;
        }

        return $is_mobile;
}

/**
 * 判断是否为微信访问
 *
 * @return boolean
 */
function is_weixin()
{
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    }
    return false;
}

/**
 * 返回带协议的域名
 */
function get_domain()
{
	$domain = Request::domain();
	return $domain;
}

/**
 * 加密方法
 * @param unknown $pw
 * @param string $authCode
 * @return string
 */
function password($pw, $authCode = '')
{
    if (empty($authCode)) {
        $authCode = config('database.auth_code');
    }
    $result = "###" . md5(md5($authCode . $pw));
    return $result;
}

/**
 * 比较:所有涉及比较
 * @param string $password 录入的需要比较的PW
 * @param string $password_in_db 数据库中保存的已经加密过的PW
 * @return boolean 相同，返回true
 */
function compare_password($password, $passwordInDb)
{
    if (strpos($passwordInDb, "###") === 0) {
        return password($password) == $passwordInDb;
    } else {
        return false;
    }
}

/**
 * 验证码检查，验证完后销毁验证码
 * @param string $value
 * @param string $id
 * @return bool
 */
function check_captcha($value, $id = "")
{
    $captcha = new Captcha();
    return $captcha->check($value, $id);
}

/**
 * 转化数据库保存图片的文件路径，为可以访问的url
 * @param string $file
 * @return string
 */
function get_image_url($file){
	if (!$file) {
		return false;
	}
	if(strpos($file,"http")===0){
		return $file;
	}else if(strpos($file,"/")===0){
		return $file;
	}else{
		$filepath=$file;
		if(strpos($filepath,"http")!==0){
			$filepath=get_domain().'/'.$filepath;
		}
		return $filepath;
	}
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
 * @return mixed
 */
function get_client_ip($type = 0, $adv = false)
{
    return request()->ip($type, $adv);
}

/**
 * 设置系统配置，通用
 * @param string $key 配置键值,都小写
 * @param array $data 配置值，数组
 * @return boolean
 */
function set_option($key, $data)
{
    if (!is_array($data) || empty($data) || !is_string($key) || empty($key)) {
        return false;
    }

    $key        = strtolower($key);
    $option     = [];
    $findOption = Db::name('option')->where('option_name', $key)->find();
    if ($findOption) {
        $oldOptionValue = json_decode($findOption['option_value'], true);
        if (!empty($oldOptionValue)) {
            $data = array_merge($oldOptionValue, $data);
        }

        $option['option_value'] = json_encode($data);

        Db::name('option')->where('option_name', $key)->update($option);
    } else {
        $option['option_name']  = $key;
        $option['option_value'] = json_encode($data);
        Db::name('option')->insert($option);
    }
    cache($key,$data);
    return true;
}

/**
 * 获取系统配置，通用
 * @param string $key 配置键值,都小写
 * @return array
 */
function get_option($key)
{
    if (!is_string($key) || empty($key)) {
        return [];
    }

    $optionValue = cache($key);

    if (empty($optionValue)) {
        $optionValue = Db::name('option')->where('option_name', $key)->value('option_value');
        if (!empty($optionValue)) {
            $optionValue = json_decode($optionValue, true);
            cache($key,$optionValue);
        }
    }

    return $optionValue;
}

/**
 * 获取单页内容
 * @param unknown $id
 * @return array|\think\db\false|PDOStatement|string|\think\Model|mixed|\think\cache\Driver|boolean
 */
function get_page($id) {
    $page = cache('page_'.$id);
    if (empty($page)) {
        $page = Db::name('article')->find($id);
        cache('page_'.$id,$page,432000);
    }

    return $page;
}

/**
 * 查询轮播图
 * @param $idname
 * @param bool $type 默认false，返回slide的items，true，则返回slide自身信息
 * @return mixed
 * @throws \think\Exception\DbException
 */
function get_slide($idname,$type = false){

    $slides = cache('slides');
    if (empty($slides)){
        $slides = \app\common\model\SlideModel::with('items',['SlideItemModel.status'=>1])->all();
        cache('slides',$slides);
    }

    $slide = list_search($slides,array('idname'=>$idname));

    if ($type)
        return $slide[0];

    return $slide[0]['items'];
}

/**
 * 发送邮件
 * @param string $address 收件人邮箱
 * @param string $subject 邮件标题
 * @param string $message 邮件内容
 * @return mixed
 * @throws \PHPMailer\PHPMailer\Exception
 */
function send_email($address, $subject, $message)
{
    $res['code'] = 1;
    $res['msg'] = 'OK';
    $smtpSetting = get_option('smtp_setting');
    $mail        = new PHPMailer();
    // 设置PHPMailer使用SMTP服务器发送Email
    $mail->IsSMTP();
    $mail->IsHTML(true);
    //$mail->SMTPDebug = 3;
    // 设置邮件的字符编码，若不指定，则为'UTF-8'
    $mail->CharSet = 'UTF-8';
    // 添加收件人地址，可以多次使用来添加多个收件人
    $mail->AddAddress($address);
    // 设置邮件正文
    $mail->Body = $message;
    // 设置邮件头的From字段。
    $mail->From = $smtpSetting['from'];
    // 设置发件人名字
    $mail->FromName = $smtpSetting['from_name'];
    // 设置邮件标题
    $mail->Subject = $subject;
    // 设置SMTP服务器。
    $mail->Host = $smtpSetting['host'];
    //by Rainfer
    // 设置SMTPSecure。
    $Secure           = $smtpSetting['smtp_secure'];
    $mail->SMTPSecure = empty($Secure) ? '' : $Secure;
    // 设置SMTP服务器端口。
    $port       = $smtpSetting['port'];
    $mail->Port = empty($port) ? "25" : $port;
    // 设置为"需要验证"
    $mail->SMTPAuth    = true;
    $mail->SMTPAutoTLS = false;
    $mail->Timeout     = 10;
    // 设置用户名和PW。
    $mail->Username = $smtpSetting['username'];
    $mail->Password = $smtpSetting['password'];
    // 发送邮件。
    if (!$mail->Send()) {
        $mailError = $mail->ErrorInfo;
        $res['code'] = 0;
        $res['msg'] = $mailError;
    } else

        return $res;
}

function array2level($array, $pid = 0, $level = 1) {
    static $list = [];
    foreach ($array as $v) {
        if ($v['pid'] == $pid) {
            $v['level'] = $level;
            $list[]     = $v;
            array2level($array, $v['id'], $level + 1);
        }
    }

    return $list;
}

/**
 * 构建层级（树状）数组
 * @param array  $array 要进行处理的一维数组，经过该函数处理后，该数组自动转为树状数组
 * @param string $pid 父级ID的字段名
 * @param string $child_key_name 子元素键名
 * @return array|bool
 */
function array2tree(&$array, $pid = 'pid', $child_key_name = 'children') {
    $counter = array_children_count($array, $pid);
    if ($counter[0] == 0)
        return false;
        $tree = [];
        while (isset($counter[0]) && $counter[0] > 0) {
            $temp = array_shift($array);
            if (isset($counter[$temp['id']]) && $counter[$temp['id']] > 0) {
                array_push($array, $temp);
            } else {
                if ($temp[$pid] == 0) {
                    $tree[] = $temp;
                } else {
                    $array = array_child_append($array, $temp[$pid], $temp, $child_key_name);
                }
            }
            $counter = array_children_count($array, $pid);
        }

        return $tree;
}

/**
 * 子元素计数器
 * @param $array
 * @param $pid
 * @return array
 */
function array_children_count($array, $pid) {
    $counter = [];
    foreach ($array as $item) {
        $count = isset($counter[$item[$pid]]) ? $counter[$item[$pid]] : 0;
        $count++;
        $counter[$item[$pid]] = $count;
    }

    return $counter;
}

/**
 * 把元素插入到对应的父元素$child_key_name字段
 * @param        $parent
 * @param        $pid
 * @param        $child
 * @param string $child_key_name 子元素键名
 * @return mixed
 */
function array_child_append($parent, $pid, $child, $child_key_name) {
    foreach ($parent as &$item) {
        if ($item['id'] == $pid) {
            if (!isset($item[$child_key_name]))
                $item[$child_key_name] = [];
                $item[$child_key_name][] = $child;
        }
    }

    return $parent;
}

/**
 * 在数据列表中搜索
 * @access public
 * @param array $list 数据列表
 * @param mixed $condition 查询条件
 * 支持 array('name'=>$value) 或者 name=$value
 * @return array
 */
function list_search($list,$condition) {
    if(is_string($condition))
        parse_str($condition,$condition);
        // 返回的结果集合
        $resultSet = array();
        foreach ($list as $key=>$data){
            $find   =   false;
            foreach ($condition as $field=>$value){
                if(isset($data[$field])) {
                    if(0 === strpos($value,'/')) {
                        $find   =   preg_match($value,$data[$field]);
                    }elseif($data[$field]==$value){
                        $find = true;
                    }
                }
            }
            if($find)
                $resultSet[]     =   &$list[$key];
        }
        return $resultSet;
}

/**
 * 数据分组
 * @param unknown $list
 * @param unknown $field
 * @return array|unknown
 * Date：2018年6月7日下午2:57:22
 * Author: tiedong
 */
function list_group_by($list,$field)
{
    $newlist = [];
    foreach ($list as $v) {
        $newlist[$v[$field]]['list'][] = $v;
    }

    foreach ($newlist as $key=>$v) {
        if (count($v['list']) > 0) {
            $newlist[$key]['count'] = count($v['list']);
        }else{
            unset($newlist[$key]);
        }
    }

    return $newlist;
}

/**
 * 获取友情链接
 * @return mixed
 */
function get_links(){
    $links = cache('friend_links');
    if (empty($links)) {
        $links = Db::name('link')->where('status',1)->order('list_order','asc')->all();
        cache('friend_links',$links,86400);
    }

    return $links;
}

/**
 * 获取前台导航
 * @return mixed
 */
function get_navs(){
    $navs = cache('frontend_navs');
    if (empty($navs)) {
        $navs = Db::name('nav')->where('status',1)->order('list_order','asc')->all();
        cache('frontend_navs',$navs,86400);
    }

    return $navs;
}

if (!function_exists('get_current_user_id')){
    /**
     * 获取当前登录的用户ID
     * @return mixed
     */
    function get_current_user_id()
    {
        return session('user.id');
    }
}