<?php

namespace app\common\validate;

use think\Validate;

class UserValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'mobile' => 'mobile|unique:user',
        'login_pwd'   => 'require|min:6|max:32',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'mobile.mobile' => '手机号格式有误',
        'mobile.unique' => '手机号必须唯一',
        'login_pwd.require'   => '密码必填',
        'login_pwd.min'  => '密码长度至少6位',
        'login_pwd.max'  => '密码长度不超过32位',
    ];

    protected $scene = [
        'register' => ['login_pwd','mobile'],
    ];
}
