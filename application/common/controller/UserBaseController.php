<?php
namespace app\common\controller;
use think\Controller;
use think\Container;

class UserBaseController extends Controller
{
    public function __construct()
    {
        $this->_initializeView();
        parent::__construct();

        $this->view  = Container::get('view')->init(
            $this->app['config']->pull('template')
        );
    }

    public function initialize() {
        parent::initialize();
        $site_info = get_option('site_info');

        if (empty(get_current_user_id()) && strtolower($this->request->controller()) != 'auth'){
            if ($this->request->isAjax()) {
                $this->error("您尚未登录", url("user/auth/login"));
            } else {
                $this->redirect(url("user/auth/login"));
            }
        }

        $this->assign(['site_info'=>$site_info,'user'=>session('user')]);
    }

    // 初始化
    protected function _initializeView()
    {
        $themeBase = 'template/';

        $frontDefaultTheme = 'frontend';

        $themePath = "{$themeBase}{$frontDefaultTheme}/";

        config ( 'template.view_base', $themePath);

        $tplReplaceStr = [
            '__PUBLIC__' => "/{$themePath}public",
            ];
        $tplReplaceStr = array_merge ( config ( 'template.tpl_replace_string' ), $tplReplaceStr );
        config ( 'template.tpl_replace_string', $tplReplaceStr );
    }

}
