<?php
namespace app\common\controller;
use think\Controller;
use think\Container;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->_initializeView();
        parent::__construct();

        $this->view  = Container::get('view')->init(
            $this->app['config']->pull('template')
        );
    }

    public function initialize() {
        parent::initialize();
        $site_info = get_option('site_info');
        $navs = get_navs();

        $this->assign(['site_info'=>$site_info,'navs'=>$navs]);
    }

    // 初始化
    protected function _initializeView()
    {
        $themeBase = 'template/';

        $frontDefaultTheme = 'frontend';

        $themePath = "{$themeBase}{$frontDefaultTheme}/";

        config ( 'template.view_base', $themePath);

        $tplReplaceStr = [
            '__PUBLIC__' => "/{$themePath}public",
            ];
        $tplReplaceStr = array_merge ( config ( 'template.tpl_replace_string' ), $tplReplaceStr );
        config ( 'template.tpl_replace_string', $tplReplaceStr );
    }

}
