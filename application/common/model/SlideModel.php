<?php
namespace app\common\model;

use think\Model;

class SlideModel extends Model
{
    public function items(){
        return $this->hasMany('SlideItemModel','slide_id');
    }

}
