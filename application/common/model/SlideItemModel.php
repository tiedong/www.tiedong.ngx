<?php
namespace app\common\model;

use think\Model;

class SlideItemModel extends Model
{
    public function slide(){
        //一对一
        return $this->belongsTo('SlideModel','slide_id');
    }
}
