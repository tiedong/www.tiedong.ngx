<?php
// +----------------------------------------------------------------------
// | SlideController.php   幻灯片管理
// +----------------------------------------------------------------------
// | Author:  tiedong
// +----------------------------------------------------------------------
// | Copyright ©2018  https://www.tiedongit.com  All rights reserved.
// +----------------------------------------------------------------------
// | Date：2018年6月2日上午8:44:05
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\common\model\SlideModel;
use app\common\model\SlideItemModel;
use think\Db;
use think\facade\Cache;

class SlideController extends AdminBaseController
{
    public function index()
    {
        return $this->fetch();
    }

    public function getRows()
    {
    	$slides = SlideModel::all();
    	$res['code'] = 0;
    	$res['msg'] = 'success';
    	$res['data'] = $slides;
    	return $res;
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $result = SlideModel::create($data);

            if ($result === false) {
                $this->error("添加失败！");
            } else {
                Cache::rm('slides');
            }
            $this->success("添加成功！");
        } else {
            return $this->fetch();
        }
    }

    public function updateSlide() {
        Cache::rm('slides');
    	return parent::fieldUpdate(new SlideModel());
    }

    public function delete()
    {
        $id = $this->request->param('id', 0, 'intval');
        SlideModel::destroy($id);
        SlideItemModel::where('slide_id',$id)->delete();

        $res['code'] = 1;
        $res['msg'] = '删除成功！';

        Cache::rm('slides');

        return $res;
    }

    /**
     * 图片管理
     * @return mixed|string
     * Date：2018年6月2日上午8:05:35
     * Author: tiedong www.zzuyxg.top
     */
    public function items()
    {
        return $this->fetch();
    }

    public function getItems()
    {
        $id  = $this->request->param('slideID',1,'intval');
        $items  = Db::name('slide_item')->where('slide_id',$id)->all();
        $res['code'] = 0;
        $res['msg'] = 'success';
        $res['data'] = $items;
        return $res;
    }

    public function updateItem() {
        Cache::rm('slides');
        return parent::fieldUpdate(new SlideItemModel());
    }

    public function itemChangeShow()
    {
        Cache::rm('slides');
        return parent::switchOperate(new SlideItemModel());
    }

    public function addItem()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $post   = $data['item'];

            $result = SlideItemModel::create($post);

            if ($result === false) {
                $this->error('添加失败！');
            }else{
                Cache::rm('slides');
            }
            $this->success('添加成功！');
        } else {
            return $this->fetch('addItem');
        }
    }

    public function editItem()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $post   = $data['slide'];

            $result = SlideItemModel::update($post);

            if ($result === false) {
                $this->error( '更新失败！');
            }else{
                Cache::rm('slides');
            }
            $this->success('更新成功！');
        } else {
            $id     = $this->request->param('id');
            $item = SlideItemModel::get($id);

            $this->assign('item', $item);
            return $this->fetch('editItem');
        }
    }

    /**
     * 删除图片并彻底删除
     * Date：2018年6月2日上午8:43:25
     * Author: tiedong www.zzuyxg.top
     */
    public function deleteItem()
    {
        $res['code'] = 1;
        $res['msg'] = '删除成功！';

        $id = $this->request->param('id', 0, 'intval');

        $thumb = Db::name('slideItem')->where('id',$id)->value('thumb');

        $result = Db::name('slideItem')->delete($id);
        if ($result) {
            Cache::rm('slides');
            if (file_exists("../public/".$thumb)){
                @unlink("../public/".$thumb);
            }
        } else {
            $res['code'] = 0;
            $res['msg'] = '删除失败！';
        }
    }

}
