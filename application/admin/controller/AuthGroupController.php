<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\admin\model\AuthGroupModel;
use app\admin\model\AuthRuleModel;
use think\Db;

/**
 * 权限组
 * Class AuthGroup
 * @package app\admin\controller
 */
class AuthGroupController extends AdminBaseController {
    /**
     * 权限组
     * @return mixed
     */
    public function index() {
        return $this->fetch('index');
    }

    public function getGroups() {
        $groups = AuthGroupModel::all();
        $res['code'] = 0;
        $res['msg'] = 'success';
        $res['data'] = $groups;
        return $res;
    }

    /**
     * 添加权限组
     * @return mixed
     */
    public function add() {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $model = new AuthGroupModel();
            $result = $model->allowField(true)->save($data);
            if ($result === false) {
                $this->error('添加失败！');
            }
            $this->success('添加成功！');
        } else {
            return $this->fetch();
        }
    }

    /**
     * 编辑权限组
     * @return mixed
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param('');
            $model = new AuthGroupModel();
            $result = $model->update($data);
            if ($result !== false) {
                $this->success("修改成功");
            } else {
                $this->error("修改失败");
            }
        } else {
            $id = input("param.id");
            $model = new AuthGroupModel();
            $data = $model->get($id);
            $this->assign(["data"=>$data]);
            return $this->fetch();
        }
    }

    public function changeShow()
    {
        $model = new AuthGroupModel();
        return parent::switchOperate($model);
    }

    public function updateField() {
        $model = new AuthGroupModel();
        return parent::fieldUpdate($model);
    }

    /**
     * 删除权限组
     */
    public function delete() {
        $id = input("param.ids");
        if($id == 1){
            $this->error('超级管理组不可删除');
        }
        if (AuthGroupModel::destroy($id)) {
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    /**
     * 授权
     * @param $id
     * @return mixed
     * @throws \think\Exception
     * @throws \think\Exception\DbException
     * @throws \think\exception\PDOException
     */
    public function setRules($id) {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $res['code'] = 1;
            $res['msg'] = '设置成功！';

            $data['rules'] = implode(',', $data['rules']);

            Db::name("auth_group")->update($data);

            return $res;
        }else{
            $group = Db::name('auth_group')->get($id);

            $group['res'] = explode(",",$group['rules']);

//            $model = new AuthRuleModel();
            $admin_menu_list       = AuthRuleModel::order('id DESC')->all();
            $admin_menu_level_list = array2level($admin_menu_list);
            foreach ($admin_menu_level_list as $key=>$vo) {
                $str = '';
                for ($i = 1; $i < $vo['level']; $i++) {
                    $str .= '---';
                }
                $admin_menu_level_list[$key]['title'] = $str.$vo['title'];
            }

            $this->assign('rules',$admin_menu_level_list);
            $this->assign('info',$group);

            return $this->fetch('setRules');
        }
    }
}
