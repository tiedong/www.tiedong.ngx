<?php
/**
用户管理
 */
namespace app\admin\controller;
use app\common\controller\AdminBaseController;
use app\common\model\UserModel;
class UserController extends AdminBaseController
{
	public function index()
	{
		return $this->fetch();

	}
	
	public function getUsers()
	{
	    $mobile = $this->request->param ( 'key.mobile','');
	    $map[]  = ['mobile','like','%'.$mobile.'%'];

	    $count = UserModel::where($map)->order('id desc')->count();
	    
	    $page = $this->request->param('page/d',1,'intval');
	    $limit = $this->request->param('limit/d',10,'intval');
	    
	    $lists = UserModel::where($map)->order("id desc")->limit(($page-1)*$limit,$limit)->all();

		$res['code'] = 0;
		$res['msg'] = 'success';
		$res['data'] = $lists;
		$res['count'] = $count;
		return $res;
	}
}