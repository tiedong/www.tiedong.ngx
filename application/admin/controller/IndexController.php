<?php
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use think\facade\Cache;

class IndexController extends AdminBaseController
{
    public function index()
    {
    	return $this->fetch('/index');
    }

    public function clearCache()
    {
        $type = $this->request->request("type");
        switch ($type) {
            case 'data' || 'all':
                \util\File::del_dir(ROOT_PATH . 'runtime' . DIRECTORY_SEPARATOR . 'cache');
                Cache::clear();
                if ($type == 'content') {
                    break;
                }

            case 'template' || 'all':
                \util\File::del_dir(ROOT_PATH . 'runtime' . DIRECTORY_SEPARATOR . 'temp');
                if ($type == 'template') {
                    break;
                }
        }
        $this->success('清理缓存');
    }
}
