<?php
// +----------------------------------------------------------------------
// | PublicController.php 管理员登录、退出、导航点击验证权限
// +----------------------------------------------------------------------
// | Author:  tiedong
// +----------------------------------------------------------------------
// | Copyright ©2018  https://www.tiedongit.com  All rights reserved.
// +----------------------------------------------------------------------
// | Date：2018年6月1日下午9:22:36
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use think\Db;

class PublicController extends AdminBaseController
{

    public function initialize()
    {}

    public function login()
    {
        $redirect = $this->request->post("redirect");
        if (empty($redirect)) {
            $redirect = $this->request->server('HTTP_REFERER');
        } else {
            $redirect = base64_decode($redirect);
        }

        session('login_http_referer', $redirect);
        if (get_current_admin_id()) {
            return redirect(url('admin/index/index'));
        } else {
            return $this->fetch("/login");
        }
    }

    public function test()
    {
        return $this->fetch("/test");
    }

    public function doLogin()
    {
        if ($this->request->isPost()) {
            $captcha = $this->request->param('captcha');
            // 验证码
            if (! check_captcha($captcha)) {
                $this->error('验证码不正确');
            }
            $name = $this->request->param("username");
            $pass = $this->request->param("password");

            $where['admin_login'] = $name;

            $result = Db::name('admin')->get($where);

            if (! empty($result)) {
                if (compare_password($pass, $result['login_pass'])) {
                    // 登入成功页面跳转
                    unset($result['login_pass']);
                    session('ADMIN_ID', $result["id"]);
                    session('admin', $result);
                    $result['last_login_ip'] = $this->request->ip(0);
                    $result['last_login_time'] = time();
                    Db::name('admin')->update($result);
                    $this->success('登陆成功！', url("Index/index"));
                } else {
                    $this->error('密码不正确！');
                }
            } else {
                $this->error('账号不存在！');
            }
        } else {
            $this->error("请求错误");
        }
    }

    /**
     * 管理员退出
     */
    public function logout()
    {
        session('admin', null);
        session('ADMIN_ID', null);
        $this->redirect('admin/public/login');
    }

    /**
     * 导航点击 检查后台用户访问权限
     * @return number
     * Date：2018年6月1日下午9:23:56
     * Author: tiedong www.zzuyxg.top
     */
    public function checkAuth()
    {
        if (session('ADMIN_ID') == 1) { // 如果不是Administrator，根据权限组ID进行验证
            return 1;
        }

        $rule = $this->request->param("rule");
        $groupID = $this->request->param("groupID");

        $rule_id = Db::name('auth_rule')->where('name', $rule)->value('id'); // 规则ID

        if ($rule_id) {
            $rules = Db::name('auth_group')->where('id', $groupID)->value('rules');
            $rules = explode(',', $rules);

            if (! in_array($rule_id, $rules)) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }
}
