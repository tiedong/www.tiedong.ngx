<?php
// +----------------------------------------------------------------------
// | AdminController.php 管理员管理
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2018 https://www.tiedongit.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: tiedong
// +----------------------------------------------------------------------
// | Date:2018年3月24日
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\common\controller\AdminBaseController;
use app\admin\model\AdminModel;
use think\Db;

class AdminController extends AdminBaseController
{
    public function index()
    {
        return $this->fetch();
    }

    /**
     * 获取管理员列表
     * @return mixed
     * @throws \think\Exception\DbException
     */
    public function getAdmins()
    {
        $model = new AdminModel();
        $lists = $model->alias('a')
            ->join("__AUTH_GROUP__ g", "a.group_id=g.id")
            ->field("a.*,g.title")
            ->all();
        foreach ($lists as $key => $value) {
            $lists[$key]['last_login_time'] = date('Y-m-d H:i:s', $value['last_login_time']);
        }
        $res['code'] = 0;
        $res['msg'] = 'success';
        $res['data'] = $lists;
        return $res;
    }

    /**
     * 添加管理员
     * @return mixed
     * @throws \think\Exception\DbException
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $res['code'] = 1;
            $data = $this->request->param('');
            if ($data['login_pass'] != $data['upass']) {
                $this->error("两次密码输入不一致");
            }
            $name = $data['admin_login'];
            $info = Db::name("admin")->where('admin_login', $name)->count();
            if ($info) {
                $this->error("该名称已被占用");
            } else {
                $data['login_pass'] = password($data['login_pass']);
                unset($data['upass']);
                $admin_id = Db::name('admin')->insertGetId($data);

                if ($admin_id) {
                    $this->success("添加成功");
                }
            }
        } else {

            $roles = Db::name("auth_group")->where("status", 1)
                ->order("id desc")
                ->all();

            $this->assign("roles", $roles);

            return $this->fetch();
        }
    }

    /**
     * 编辑管理员
     * @return mixed
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param('');
            if(empty($data['login_pass'])) unset($data['login_pass']);
            if(empty($data['upass'])) unset($data['upass']);
            $result = Db::name("admin")->update($data);

            if ($result !== false) {
                $this->success("修改成功");
            } else {
                $this->error("修改失败");
            }
        } else {
            $id = input("param.id");
            $data = Db::name("admin")->get($id);
            $roles = Db::name("auth_group")->where("status = 1")->all();
            $this->assign(["roles"=>$roles,"data"=>$data]);
            return $this->fetch();
        }
    }

    /**
     * 删除管理员
     */
    public function delAdmin()
    {
        $id = input("param.ids");
        Db::name("admin")->delete($id);
        $this->success("删除成功");
    }

    /**
     * 编辑资料
     * @return mixed
     */
    public function editInfo()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $result = Db::name('admin')->update($data);
            ;
            if ($result !== false) {
                $this->success("保存成功！");
            } else {
                $this->error("保存失败！");
            }
        } else {
            $id = get_current_admin_id(); // TODO
            $id = isset($id) ? $id : 1;
            $admin = Db::name('admin')->get($id);
            $this->assign('admin', $admin);
            return $this->fetch('editInfo');
        }
    }

    /**
     * 修改状态
     */
    public function updateStatus()
    {
        $data['id'] = input("param.id");
        $data['status'] = input("param.status");
        $result = Db::name("admin")->update($data);
        if ($result !== false) {
            $this->success("更新状态成功");
        } else {
            $this->error("更新状态失败");
        }
    }
}
