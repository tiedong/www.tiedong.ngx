<?php
// +----------------------------------------------------------------------
// | 网站设置
// +----------------------------------------------------------------------
// | Author:  tiedong
// +----------------------------------------------------------------------
// | Copyright ©2018  https://www.tiedongit.com  All rights reserved.
// +----------------------------------------------------------------------
// | 2018年3月23日
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\Db;
use app\common\controller\AdminBaseController;

class SettingController extends AdminBaseController
{
    public function site()
    {
        if ($this->request->isPost()) {
            $options = $this->request->param('options/a');
            set_option('site_info', $options);
            $this->success("保存成功！",'');
        }else{
            $this->assign(get_option('site_info'));
            return $this->fetch();
        }
    }

    public function password()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            if (empty($data['old_password'])) {
                $this->error("原始密码不能为空！");
            }
            if (empty($data['password'])) {
                $this->error("新密码不能为空！");
            }

            $userId = get_current_admin_id();

            $admin = Db::name('admin')->find($userId);

            $oldPassword = $data['old_password'];
            $password    = $data['password'];
            $rePassword  = $data['re_password'];

            if (compare_password($oldPassword, $admin['login_pass'])) {
                if ($password == $rePassword) {
                    if (compare_password($password, $admin['login_pass'])) {
                        $this->error("新密码不能和原始密码相同！");
                    } else {
                        Db::name('admin')->where('id', $userId)->update(['login_pass' => password($password)]);
                        $this->success("密码修改成功！");
                    }
                } else {
                    $this->error("密码输入不一致！");
                }

            } else {
                $this->error("原始密码不正确！");
            }
        }else{
            return $this->fetch();
        }
    }

    public function clearCache()
    {
        clear_cache();
        return $this->fetch();
    }

}
