<?php
// +----------------------------------------------------------------------
// | 后台需要使用到的函数库
// +----------------------------------------------------------------------
// | Author: tiedong
// +----------------------------------------------------------------------
// | Copyright ©2017 https://www.tiedongit.com All rights reserved.
// +----------------------------------------------------------------------
// | 2018年3月23日
// +----------------------------------------------------------------------
/**
 * 获取当前登录的管理员ID
 * 
 * @return int
 */
function get_current_admin_id()
{
    return session('ADMIN_ID');
}

/**
 * scan_dir的方法
 * @param string $pattern 检索模式 搜索模式 *.txt,*.doc; (同glog方法)
 * @param int $flags
 * @param $pattern
 * @return array
 */
function scan_dir($pattern, $flags = null)
{
    $files = array_map('basename', glob($pattern, $flags));
    return $files;
}

/**
 * 清空系统缓存
 */
function clear_cache()
{
    $dirs     = [];
    $rootDirs = scan_dir(RUNTIME_PATH . "*");
    $noNeedClear = [".", ".."];
    $rootDirs    = array_diff($rootDirs, $noNeedClear);
    foreach ($rootDirs as $dir) {
        if ($dir != "." && $dir != "..") {
            $dir = RUNTIME_PATH . $dir;
            if (is_dir($dir)) {
                //array_push ( $dirs, $dir );
                $tmpRootDirs = scan_dir($dir . "/*");
                foreach ($tmpRootDirs as $tDir) {
                    if ($tDir != "." && $tDir != "..") {
                        $tDir = $dir . '/' . $tDir;
                        if (is_dir($tDir)) {
                            array_push($dirs, $tDir);
                        } else {
                            @unlink($tDir);
                        }
                    }
                }
            } else {
                @unlink($dir);
            }
        }
    }
    $dirTool = new Directory("");
    foreach ($dirs as $dir) {
        $dirTool->delDir($dir);
    }
}

?>