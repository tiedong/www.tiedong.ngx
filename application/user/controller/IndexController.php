<?php
namespace app\user\controller;

use app\common\controller\UserBaseController;

class IndexController extends UserBaseController
{
    public function index()
    {
        return $this->fetch('/index');
    }
}
