<?php
namespace app\user\controller;

use app\common\controller\UserBaseController;

class ProfileController extends UserBaseController
{
    public function index()
    {
        return $this->fetch();
    }

    public function setting()
    {
        return $this->fetch();
    }

    public function favorite()
    {
        return $this->fetch();
    }

    public function comment()
    {
        return $this->fetch();
    }

    public function score()
    {
        return $this->fetch();
    }
}