<?php
// +----------------------------------------------------------------------
// | AssetController.php 文件上传接口
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2018  https://www.tiedongit.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: tiedong
// +----------------------------------------------------------------------
// | Date:2019年5月14日
// +----------------------------------------------------------------------
namespace app\api\controller;

use app\common\controller\BaseController;
use think\Db;

class AssetController extends BaseController
{
    // 文件上传
    public function fileUpload()
    {
        if ($this->request->isPost()) {
            $res['code'] = 1;
            $res['msg'] = '上传成功！';

            $file = $this->request->file('file');

            $path = $this->request->param('path');

            if (!$path)
                $path = date('Ymd',time());

            $info = $file->rule('uniqid')->move('../public/upload/'.$path.'/');
            if ($info) {
                $fileInfo = $info->getInfo();
                $res['name'] = $fileInfo['name'];
                $res['filepath'] = '/upload/'.$path.'/'.$info->getSaveName();

                Db::name('asset')->insert(['path'=>$path,'file_name'=>$fileInfo['name'],'type'=>$fileInfo['type'],'size'=>$fileInfo['size'],'file_path'=>'/upload/'.$path.'/'.$info->getSaveName()]);
            } else {
                $res['code'] = 0;
                $res['msg'] = '上传失败！'.$file->getError();
            }
            return $res;
        }
    }

    /**
     * ueditor富文本编辑器上传接口
     * User: Eric
     * DateTime: 2019/5/14 23:06
     * @return \think\response\Json
     */
    public function umeUpload()
    {
        if ($this->request->isPost()) {
            $res['state'] = 'SUCCESS';
            $res['msg'] = '上传成功！';

            $file = $this->request->file('upfile');
            $date = date('Ymd',time());
            $info = $file->rule('uniqid')->move('../public/upload/umeditor/'.$date.'/');
            if ($info) {
                $res['url'] = 'upload/umeditor/'.$date.'/'.$info->getSaveName();
            } else {
                $res['state'] = 'ERROR';
            }

            return json($res);
        }
    }
}
