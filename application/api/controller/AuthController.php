<?php

namespace app\api\controller;

use app\common\model\UserModel;
use app\common\validate\UserValidate;
use think\Controller;
use think\Db;

class AuthController extends Controller
{
    public function doLogin()
    {
        if ($this->request->isPost()) {
            $name = $this->request->param('username', '');
            $pass = $this->request->param('password');

            $result = Db::name('user')->where('mobile', $name)->find();

            if (!empty($result)) {
                if (compare_password($pass, $result['login_pwd'])) {

                    unset($result['login_pwd']);

                    session('user', $result);

                    $update['id'] = $result['id'];
                    $update['last_login_ip'] = $this->request->ip();
                    $update['last_login_time'] = time();

                    Db::name('user')->update($update);

                    return ['code'=>1,'msg'=>'验证通过'];
                } else {
                    return ['code'=>0,'msg'=>'密码不正确'];
                }
            } else {
                return ['code'=>0,'msg'=>'账号不存在'];
            }
        } else {
            return ['code'=>0,'msg'=>'非法请求'];
        }
    }

    public function doRegister()
    {
        if ($this->request->isPost()) {

            $data = $this->request->only(['mobile','login_pwd']);

            $validate = new UserValidate();
            $result = $validate->check($data);

            if (!$result)
                return ['code'=>0,'msg'=>$validate->getError()];

            $data['login_pwd'] = password($data['login_pwd']);

            $data['create_time'] = time();
            $data['create_ip'] = $this->request->ip();

            Db::startTrans();
            try {
                $user = UserModel::create($data);

                $uid = $user->id;

                $spread_uid = $this->request->param('spread_uid', 0);

                UserModel::setSpread($spread_uid, $uid);

                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
            }
            return ['code'=>1,'msg'=>'注册成功'];
        } else {
            return ['code'=>0,'msg'=>'非法请求'];
        }
    }
    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @Date 2018年11月30日
     * @Author tiedong
     */
    public function onLogin(){
        $code = $this->request->param('code','');
        $AppID = '';
        $AppSecret = '';
        if ($code) {
            $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$AppID.'&secret='.$AppSecret.'&js_code='.$code.'&grant_type=authorization_code';
            
            //初始化curl
            $ch = curl_init();
            //设置超时
            //curl_setopt($ch, CURLOPT_TIMEOUT, $this->curl_timeout);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,FALSE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            //运行curl，结果以json形式返回
            $result = curl_exec($ch);
            
            curl_close($ch);
            $result= json_decode($result,true);
            
            $res['result'] = $result;

            $find = Db::name('user')->where('openid',$result['openid'])->find();
            if ($find) {
                $uid = $find['id'];
            }else{
                $data = [
                    'openid' => $result['openid'],
                    'subscribe_time' => time()
                ];
                
                $uid = Db::name('user')->insertGetId($data);
            }
            
            $res['uid'] = $uid;
        }else{
            $res['code'] = 0;
            $res['msg'] = '未获取到code';
        }
        
        return $res;
    }
}
