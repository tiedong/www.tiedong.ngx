# tiedongCms-基于ThinkPHP5.1(LTS版本)和layui的内容管理系统

### 项目介绍
工作之余，有些自己的想法，就基于ThinkPHP5.1，和前端框架LayUI，开发TIEDONG内容管理系统，其实也算不上CMS，不过一些项目可以在此基础上开发，可以节省一些工作。
比如，后台管理系统已搭建好，使用的是layui，已实现部分功能，比如单页模块、内容模块、图片模块、在线留言、友情链接、会员与权限管理等。以及部分前台功能，会员注册、登录、小程序授权等相关功能。
开发过程中，借鉴了很多优秀的CMS系统的功能以及特点。

### 软件架构
基于ThinkPHP 5.1（LTS版本）开发，前框框架LayUI 2.4.5。

> 注意：ThinkPHP5的运行环境要求PHP5.6以上，建议PHP7以上。

### 安装教程
1. 创建数据库比如tiedong，运行根目录的tiedong.sql初始化。
2. 其他部署操作参考ThinkPHP即可。

### 参考手册
ThinkPHP在线手册

+ [完全开发手册](https://www.kancloud.cn/manual/thinkphp5_1/content)

### 目录结构
目录结构与ThinkPHP保持一致，除了应用以及视图目录有所调整
~~~
www  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─common             公共模块目录
│  │  ├─controller      Base控制器目录
│  │  ├─model           公共模型目录
│  │
│  ├─admin              后台管理模块
│  │  ├─common.php      模块函数文件
│  │  ├─controller      控制器目录
│  │  ├─model           模型目录
│  │  ├─validate        验证器目录
│  │  └─ ...            更多类库目录
│  │
│  ├─api                API接口模块
│  │  ├─config          模块配置信息
│  │  ├─controller      控制器目录
│  │  └─ ...            更多类库目录
│  │
│  ├─index              前台模块
│  │  ├─controller      控制器目录
│  │  └─ ...            更多类库目录
│  │
│  ├─command.php        命令行定义文件
│  ├─common.php         公共函数文件
│  └─tags.php           应用行为扩展定义文件
│
├─config                应用配置目录
│  ├─module_name        模块配置目录
│  │  ├─database.php    数据库配置
│  │  ├─cache           缓存配置
│  │  └─ ...            
│  │
│  ├─app.php            应用配置
│  ├─cache.php          缓存配置
│  ├─cookie.php         Cookie配置
│  ├─database.php       数据库配置
│  ├─log.php            日志配置
│  ├─session.php        Session配置
│  ├─template.php       模板引擎配置
│  └─trace.php          Trace配置
│
├─route                 路由定义目录
│  ├─route.php          路由定义
│  └─...                更多
│
├─public                WEB目录（对外访问目录）
│  ├─static             静态资源目录
│  ├─template           前后台HTML模板目录
│  ├─favicon.ico        网页图标，直接替换即可
│  ├─index.php          入口文件
│  ├─router.php         快速测试文件
│  └─.htaccess          用于apache的重写
│
├─thinkphp              框架系统目录
│  ├─lang               语言文件目录
│  ├─library            框架类库目录
│  │  ├─think           Think类库包目录
│  │  └─traits          系统Trait目录
│  │
│  ├─tpl                系统模板目录
│  ├─base.php           基础定义文件
│  ├─console.php        控制台入口文件
│  ├─convention.php     框架惯例配置文件
│  ├─helper.php         助手函数文件
│  ├─phpunit.xml        phpunit配置文件
│  └─start.php          框架入口文件
│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─vendor                第三方类库目录（Composer依赖库）
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
~~~